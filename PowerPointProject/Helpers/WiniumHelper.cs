﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using AventStack.ExtentReports;

namespace PowerPointProject.Helpers
{
    public static class WiniumHelper

    {

        #region Find Elements

        public static IWebElement FindElement(By by)

        {

            try

            {

                return DriverManager.Driver.FindElement(by);

            }

            catch(Exception ex)

            {
                Console.WriteLine(ex);
                return null;

            }

        }



        public static IWebElement FindElement(IWebElement ancestor, By by)

        {

            try

            {

                return ancestor.FindElement(by);

            }

            catch

            {

                return null;

            }

        }



        public static IWebElement FindElement(By ancestor_By, By by)

        {

            try

            {

                return FindElement(ancestor_By).FindElement(by);

            }

            catch

            {

                return null;

            }

        }

        #endregion



        #region Elements status

        public static Status ElementIsExists(By by)

        {

            return FindElement(by) == null ? Status.Fail : Status.Pass;

        }

        #endregion



        #region Actions

        public static Status OnClick(By by)

        {

            try

            {

                FindElement(by).Click();

                return Status.Pass;

            }

            catch

            {

                return Status.Fail;

            }

        }

        public static Status Write(By by, String text)

        {

            try

            {

                FindElement(by).SendKeys(text);

                return Status.Pass;

            }

            catch(Exception ex)

            {
                Console.WriteLine(ex);
                return Status.Fail;

            }

        }

        public static string GetTextFromElement(By by)

        {

            try

            {

                return FindElement(by).Text;

            }

            catch

            {

                return null;

            }

        }



        public static Status OnClick(IWebElement ancestor, By by)

        {

            try

            {

                FindElement(ancestor, by).Click();

                return Status.Pass;

            }

            catch

            {

                return Status.Fail;

            }

        }



        public static Status OnClick(By ancestor_By, By by)

        {

            try

            {

                FindElement(ancestor_By, by).Click();

                return Status.Pass;

            }

            catch

            {

                return Status.Fail;

            }

        }

        #endregion

    }
}
