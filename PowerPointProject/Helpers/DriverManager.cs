﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium.Remote;

namespace PowerPointProject.Helpers
{
    //[SetUpFixture]

    public static class DriverManager

    {

        
        public static RemoteWebDriver Driver { get; set; }


        //[SetUp]

        public static void Create()

        {
             
            var dc = new DesiredCapabilities();

            dc.SetCapability("app", @"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\PowerPoint.lnk");
             
            Driver = new RemoteWebDriver(new Uri("http://127.0.0.1:9999"), dc);

            Thread.Sleep(TimeSpan.FromMinutes(0.1));

        }

    }
}
