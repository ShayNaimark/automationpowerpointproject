﻿using AventStack.ExtentReports;
using OpenQA.Selenium;
using PowerPointProject.Helpers;
using PowerPointProject.Infra.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerPointProject.Infra.Pages
{
    class BaseSlide
    {
        public FontSection fontSection;
        public SlideObject slide;

        public BaseSlide()
        {
            fontSection = new FontSection();
            slide = new SlideObject();
        }

        public Status ReadDataAndCreateSlide(SlideObject slideObject)
        {
            try
            {
                WiniumHelper.Write(By.Name("Title TextBox"), slideObject.Title);
                WiniumHelper.Write(By.Name("Content Placeholder"), slideObject.Values["subTitle"] + " " + slideObject.Values["text"]);
                
                /*
                foreach (var slide in slideObject.Values)
                {
                    WiniumHelper.Write(By.Name("Content Placeholder"), slide.Value);

                }
                */

                WiniumHelper.OnClick(By.Name("New Slide"));

                return Status.Pass;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return Status.Fail;
            }
        }

        public Status ReadDataAndCreateTheFirstSlide(string presentationName, string writer)
        {
            try
            {
                WiniumHelper.Write(By.Name("Title TextBox"), presentationName);
                WiniumHelper.Write(By.Name("Subtitle TextBox"), writer);
                WiniumHelper.OnClick(By.Name("New Slide"));
                
                return Status.Pass;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return Status.Fail;
            }
        }
    }
}
