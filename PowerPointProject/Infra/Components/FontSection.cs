﻿using AventStack.ExtentReports;
using OpenQA.Selenium;
using PowerPointProject.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PowerPointProject.Infra.Components
{
    class FontSection
    {
        public Status PressBoldBtn()
        {
            try
            {
                WiniumHelper.OnClick(By.Name("Title TextBox"));
                WiniumHelper.OnClick(By.Name("Bold"));
                return Status.Pass;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                return Status.Fail;
            }
        }

        public Status SetFontSize(string size)
        {
            try
            {
                Status sizee = WiniumHelper.OnClick(By.XPath("//*[@Name='Home']//*[@Name='Font']//*[@Name='Font Size']//*[@Name='Open']")); // working
                //Status sizee1 = WiniumHelper.OnClick(By.XPath($"//*[@Name='Home']//*[@Name='Font']//*[@Name='Font Size']//*[@Name='Open']//*[contains(@Name,'{size}')]")); // needs a check
                return Status.Pass;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                return Status.Fail;
            }
        }

        public Status SetFontType(string fontFamily)
        {
            try
            {
                Status fontFamilyy = WiniumHelper.OnClick(By.XPath("//*[@Name='Home']//*[@Name='Font']//*[@Name='Font']//*[@Name='Open']//*[@Name='All Fonts']")); // working
                //Status fontFamilyy1 = WiniumHelper.OnClick(By.XPath($"//*[@Name='Home']//*[@Name='Font']//*[@Name='Font']//*[@Name='Open']//*[@Name='All Fonts']//*[contains(@Name,'{fontFamily}')]")); // needs a check
                return Status.Pass;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return Status.Fail;
            }
        }

        public Status SetFontColor(string color)
        {
            try
            {

                Status fontColor = WiniumHelper.OnClick(By.XPath("//*[@Name='Home']//*[@Name='Font']//*[@Name='Font Color']//*[@Name='More Options']")); // working
                //Status fontColor1 = WiniumHelper.OnClick(By.XPath($"//*[@Name='Home']//*[@Name='Font']//*[@Name='Font Color']//*[@Name='More Options']//*[contains(@Name,'{color}')]")); // needs a check
                Thread.Sleep(5000);
                return Status.Pass;
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return Status.Fail;
            }
        }

       
        
        //WiniumHelper.FindElement(By.XPath("//*[@Name='Font']//*[@Name='Font Color']//*[@Name='More Options']"));

       
        //Status shjh = WiniumHelper.OnClick(By.XPath("//*[@Name='Home']//*[@Name='Font']//*[@Name='Font Color']//*[@Name='More Options']/../Menu[@Name = 'Font Color']/Pane[@Name='Font Color']/Data Grid[@Name='Font Color']"));


        //WiniumHelper.FindElement().FindElement("./*[]")
        //Status sfh = WiniumHelper.OnClick(By.XPath("//*[@Name='Font Color']//*[@Name='Font Color']//*[contains(@Name,'Blue')]"));

        //Status safh = WiniumHelper.OnClick(By.XPath("//*[@Name='Font Color']//*[@Name='Font Color']//*[@Name='Font Color']//*[contains(@Name,'Blue')]"));

        //Status sassfh = WiniumHelper.OnClick(By.XPath("//*[@Name='Font Color']//*[@Name='Font Color']//*[@Name='Font Color']//*[@Name='Font Color']//*[contains(@Name,'Blue')]"));

        //"//Pane[@Name='Desktop 1']/Window[@Name='Presentation 1 - PowerPoint']/Pane[@Name='MsoDock']/Pane[@Name='Ribbon']/Pane[@Name='Ribbon']/Pane[@Name='Lower Ribbon']/Group/[@Name='Home']/Group/[@Name='Font']/SplitButton[@Name='Font Color']/Menu[@Name='Font Color']/Pane[@Name='Font Color']/Data Grid[@Name='Font Color']/List Item[@Name='Light Blue']";
    }
}
