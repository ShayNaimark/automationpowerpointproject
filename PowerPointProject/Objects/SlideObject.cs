﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using PowerPointProject.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AventStack.ExtentReports;

namespace PowerPointProject.Infra.Components
{
    public class SlideObject
    {
        private string title;
        private Dictionary<string, string> values = new Dictionary<string, string>();

    public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public Dictionary<string, string> Values
        {
            get { return values; }
            set { values = value; }
        }
    
    }
}                
        
