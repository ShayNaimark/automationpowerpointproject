﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AventStack.ExtentReports;
using PowerPointProject.Infra.Components;

namespace PowerPointProject.Helpers
{
    public sealed class PresentationDetailsObject
    {
        private string presentationName;
        private string writer;
        private List<SlideObject> listOfSlidesObjects = new List<SlideObject>();
        private static PresentationDetailsObject instance = null;

        public static PresentationDetailsObject GetInstance
        {
            get
            {
                if (instance == null)
                    instance = new PresentationDetailsObject();
                return instance;
            }
        }

        public string PresentationName
        {
            get { return presentationName; }
            set { presentationName = value;  }
        }

        public string Writer
        {
            get { return writer; }
            set { writer = value; }
        }

       
        public List<SlideObject> ListOfSlidesObjects
        {
            get { return listOfSlidesObjects; }
            set { listOfSlidesObjects = value; }
        }
    }
}


