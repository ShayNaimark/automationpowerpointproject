﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AventStack.ExtentReports;
using PowerPointProject.Helpers;
using PowerPointProject.Infra.Pages;

namespace PowerPointProject.Process
{
    class CreateAndDesignPresentationProcess
    {
        public BaseSlide baseSlide;
        public ChooseDocument chooseDocument;

        public CreateAndDesignPresentationProcess()
        {
            baseSlide = new BaseSlide();
            chooseDocument = new ChooseDocument();
        }

        //return dictionary from this function, the process is list of commands and the test is seperated into steps
        public Dictionary<string, Status> CreateAndDesignAutomaticPresentationProcess()
        {
            var results = new Dictionary<string, Status>();
            try
            {
                results.Add("Press Bold Btn", baseSlide.fontSection.PressBoldBtn());
                //results.Add("Set Font Color", baseSlide.fontSection.SetFontColor("Blue"));
                //results.Add("Set Font Type", baseSlide.fontSection.SetFontType("Tahoma"));
                //results.Add("Set Font Size", "baseSlide.fontSection.SetFontSize("48")");
                return results;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return results;
            }

        }
    }
}