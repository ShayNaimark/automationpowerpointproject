﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using PowerPointProject.Helpers;
using System.IO;
using PowerPointProject.Infra.Components;
using PowerPointProject.Infra.Pages;
using PowerPointProject.Process;
using Newtonsoft.Json;
using AventStack.ExtentReports;

namespace PowerPointProject.Tests
{

    [TestFixture]

    public class Test
    {
        [Test]
        public void OpenPowerPoint()
        {
            try
            {
                PresentationDetailsObject presentationDetailsObject = PresentationDetailsObject.GetInstance;
                presentationDetailsObject = JsonHelper.ReadFromJsonToObject<PresentationDetailsObject>(@"C:\Users\p0028651\Desktop\Shay\automationpowerpointproject\PowerPointProject\TestData\Inputs.json");
                CreateAndDesignPresentationProcess createAndDesignPresentationProcess = new CreateAndDesignPresentationProcess();
                createAndDesignPresentationProcess.chooseDocument.ChooseDocumentType();
                createAndDesignPresentationProcess.baseSlide.ReadDataAndCreateTheFirstSlide(presentationDetailsObject.PresentationName, presentationDetailsObject.Writer);
                Dictionary<string, Status> results = createAndDesignPresentationProcess.CreateAndDesignAutomaticPresentationProcess();

                //loop over the list of the slides
                foreach (SlideObject item in presentationDetailsObject.ListOfSlidesObjects)
                {
                    createAndDesignPresentationProcess.baseSlide.ReadDataAndCreateSlide(item);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
