﻿using NUnit.Framework;
using PowerPointProject.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerPointProject.Tests
{
    [SetUpFixture]

    class BaseTest
    {
        [OneTimeSetUp]
        public void BeforeTest()
        {
            DriverManager.Create();
        }

        
        [OneTimeTearDown]
        public void TearDown()

        {
            DriverManager.Driver.Quit();
        }
    }
}
